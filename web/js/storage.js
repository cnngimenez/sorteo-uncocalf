/*
  Copyright (C) 2024  cnngimenez

  storage.js

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const dbname = 'participantes_storage';
let db;

function st_guardar(lst, what_to_do) {
    const object_store = db.transaction([dbname], 'readwrite').objectStore(dbname);
    const request = object_store.put({id: 1, data: lst});
    request.onsuccess = (evt) => {
        console.log('IndexDB object store succeded.');
        if (what_to_do != undefined || what_to_do != null) {
            what_to_do(lst);
        }
    };    
}

function st_cargar(what_to_do) {
    const object_store = db.transaction([dbname], 'readwrite').objectStore(dbname);
    const request = object_store.get(1);
    
    request.onsuccess = (evt) => {
        var data = [];
        console.log('IndexDB object load succeded.');

        if (what_to_do != undefined || what_to_do != null) {
            if (request.result != undefined && request.result != null) {
                data = request.result.data;
            }
            what_to_do(data);
        }
    };
}

/**
 Open the InedxedDB.

 @param what_to_do A function to execute when the DB is successfully opened.
 */
function st_open(what_to_do) {
    const open_request = window.indexedDB.open(dbname, 1);
    open_request.onsuccess = (evt) => {
        console.log('IndexDB: DB opened successfully.');
        db = open_request.result;
        if (what_to_do != undefined || what_to_do != null) {
            what_to_do();
        }
    };
    open_request.onupgradeneeded = (evt) => {
        console.log('IndexDB: Upgrading DB.');
        
        db = evt.target.result;
        const object_store = db.createObjectStore(dbname, { keyPath: 'id' });
    };
}
