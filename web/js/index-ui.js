/*
  Copyright (C) 2024  cnngimenez

  index-ui.js

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const randomURL = 'https://controlz.fi.uncoma.edu.ar/sorteo-uncocalf/random.php';
const enavigator = document.querySelector('#navigator');

var lst_participantes = [];

function existe_participante(nombre, cifras) {
    return buscar_participante(nombre, cifras) >= 0;
}

function buscar_participante(nombre, cifras) {
    return lst_participantes.findIndex(
        (elt) => {
            return elt.nombre == nombre && elt.cifras == cifras;
        });
}

function remover_participante(nombre, cifras) {
    const index = buscar_participante(nombre, cifras);
    if (index < 0) {
        return;
    }

    lst_participantes.splice(index, 1);
}

function sortear_remotamente() {
    return fetch(randomURL + '?max=' + lst_participantes.length)
        .then((resp) => {
            if (!resp.ok) {
                ons.notification.alert('¡No se pudo conectar a Internet!');
                return -1;
            }
            return resp.text();
        }).
        then((resp) => {
            return parseInt(resp);
        });
}

/** 
 @return Índice del participante ganador.
 */
function sortear_localmente() {
    return Math.floor(Math.random() * lst_participantes.length);    
}

function mostrar_ganador(rindex, borrar) {   
    const ganador = lst_participantes[rindex];
    
    ons.notification.alert('¡Ganador: ' + ganador.nombre + ' - ' + ganador.cifras + '!');
    
    if (borrar) {
        remover_onclick(ganador.nombre, ganador.cifras);
    }
}

function sortear_onclick(local, borrar) {
    if (lst_participantes.length == 0) {
        ons.notification.alert('¡No se puede sortear sin un participante!');
        return;
    }

    if (local) {
        const rindex = sortear_localmente();
        mostrar_ganador(rindex, borrar);
    } else {
        sortear_remotamente().then((rindex) => {
            mostrar_ganador(rindex, borrar); 
        });
    }
}

function remover_onclick(nombre, cifras) {
    remover_participante(nombre, cifras);
    const eitem = document.querySelector(
        'ons-list-item[data-nombre="' + nombre + '"][data-cifras="' + cifras + '"]'
    );

    eitem.remove();
    update_db();
}

function borrar_todos_onclick() {
    const elist = document.querySelector('#participantes-list');
    lst_participantes = [];
    elist.innerHTML = '';
    update_db();
}

function borrar_formulario() {
    const enombre = document.querySelector('#nombre');
    const ecifras = document.querySelector('#cifras');
    enombre.value = '';
    ecifras.value = '';
}

function agregar_onclick() {
    const nombre = document.querySelector('#nombre').value.trim();
    const cifras = document.querySelector('#cifras').value.trim();
    var veces = document.querySelector('#veces').value;
    if (veces == "") {
        veces = 1;
    }
    
    if (nombre == "" || cifras == "") {
        ons.notification.alert('¡Debe escribir un nombre y sus cifras!');
        return;
    }
    
    if (! existe_participante(nombre, cifras)) {    
        agregar_participante(nombre, cifras, veces);
        borrar_formulario();
    } else {
        ons.notification.alert('¡Ya existe el participante!');
    }
}

function lst_agregar_item(nombre, cifras) {
    const html = '<div class="center">' +
          nombre + ' - ' + cifras + '</div>' +
          '<div class="right"><ons-button onclick="remover_onclick(\'' + nombre + '\', \'' + cifras + '\')">Remover</ons-button> </div>';
    const elist = document.querySelector('#participantes-list');
    const eitem = document.createElement('ons-list-item');

    eitem.setAttribute('data-nombre', nombre);
    eitem.setAttribute('data-cifras', cifras);
    
    eitem.innerHTML = html;    
    elist.append(eitem);

}
function agregar_participante(nombre, cifras, veces, notoast, nosave) {
    for (i =1; i <= veces; i++){
        lst_agregar_item(nombre, cifras);
        if (nosave != true) {
            lst_participantes.push({'nombre': nombre, 'cifras': cifras}) - 1;
        }
    }
    
    if (nosave != true) {
        update_db();
    }
    if (notoast != true) {
        ons.notification.toast('¡Participando!', { timeout: 2000 });
    }
}

function update_participantes() {
    const elist = document.querySelector('#participantes-list');
    elist.innerHTML = '';
    lst_participantes.forEach( (participante) => {
        agregar_participante(participante.nombre, participante.cifras, 1, true, true);
    });
}

function update_db() {
    st_guardar(lst_participantes);
}

function update_exportar_text() {
    const eexport = document.querySelector('textarea#exportar');
    if (eexport != undefined && eexport != null) {
        eexport.value = JSON.stringify(lst_participantes);
    }
}

function exportar_onclick() {    
    enavigator.pushPage('page-exportar.html').then( () => {
        update_exportar_text();
    });
}

function exportar_copy_onclick() {
    let jsonstr = document.querySelector('textarea#exportar').value;
    navigator.clipboard.writeText(jsonstr).then(
        () => {
            ons.notification.toast('¡Datos copiados!', {timeout: 2000});
        },
        (err) => {
            ons.notification.toast('No se pudo copiar los datos. Intente manualmente.',
                                   {timeout: 2000});
        });
}

function importar_paste_onclick() {
    try {
        navigator.clipboard.readText().then( (text) => {
            let elt = document.querySelector('textarea#importar');
            elt.value = text;        
        });
    } catch {
        ons.notification.toast('Hubo problemas al pegar los datos. Intente manualmente.',
                               {timeout: 2000});
    }
}

/**
 Chequear si data tiene el formato necesario para lst_participantes.
 */
function chequear_data(data) {
    return data instanceof Array &&
        data.every( (elt) => {
            return elt instanceof Object &&
                elt['nombre'] != undefined &&
                elt['cifras'] != undefined;
        });
}

function importar_onclick() {
    try {
        const txt = document.querySelector('textarea#importar').value;
        const data = JSON.parse(txt);

        if (!chequear_data(data)) {
            ons.notification.toast('Los datos no tienen el formato correcto',
                                   {timeout: 2000});
            return;
        }
        
        lst_participantes = data;
        update_db();
        update_participantes();
        update_exportar_text();
        
        ons.notification.toast('¡Importación exitosa!',
                               {timeout: 2000});
    } catch {
        ons.notification.toast('Error al importar.',
                               {timeout: 2000});
    }
}

function startup() {
    st_open(() => {
        st_cargar((data) => {
            lst_participantes = data;
            update_participantes();
        });
    });
}

ons.ready(startup);
