<?php
/**
  Copyright (C) 2024  cnngimenez

  random.php

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

  PHP version >7

  @category Main
  @package  Main
  @author   Christian Gimenez
  @license  AGPLv3 https://gnu.org/licenses
  @link     https://gitlab.com/cnngimenez/sorteo-uncocalf
*/

// GPG may not execute in some environments... This implementation is commentend just in favor of random_int.
/*
 $output = null;
 $retval = null;
 $l = exec("gpg2 --gen-random 2 2", $output, $retval);
 $rnum = ord($l);

 if (array_key_exists('max', $_GET)) {
   echo $rnum % (int) $_GET['max'];
 } else {
   echo $rnum;
 }
*/

// According to PHP Manual, random_int uses getrandom(), which is cryptographic secure...
if (array_key_exists('max', $_GET)) {
    echo random_int(0, $_GET['max']);
} else {
    echo random_int(0, 65535);
}
